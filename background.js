/************************************************************************************
  This is your background code.
  For more information please visit our wiki site:
  http://docs.crossrider.com/#!/guide/scopes_background
*************************************************************************************/

appAPI.ready(function($) {
    // Add the icon from the Resources folder
    // See the note following this code.
    appAPI.browserAction.setResourceIcon('icon.png');
    
	appAPI.browserAction.onClick(function() {
		appAPI.message.toActiveTab({action:'openWhisk'});
		return;
	});
});
