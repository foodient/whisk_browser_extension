/************************************************************************************
  This is your Page Code. The appAPI.ready() code block will be executed on every page load.
  For more information please visit our docs site: http://docs.crossrider.com
*************************************************************************************/

appAPI.ready(function($) {
	// Constants for whitelisted recipe sites (compare with window.location.hostname)
	//var FOODNETWORK = "foodnetwork";
	var whiteList = [
		//FOODNETWORK
	];
	
	// Is it using hRecipe?
	var usesHRecipe = findHRecipe();
	// Is it using Schema.org?
	var usesMicroformat = findMicroformat();
	// Is it in whitelist?
	var isInWhiteList = findCurrentUrlInRecipeWhiteList();
	
	var rootUrl = decideRootUrl();

	var currentUrl = window.location.href;
	var sidebarOpen = false;
	var sidebar;
	var appId = appAPI.appInfo.id;
	var queryData = { 'url': currentUrl, 'click_origin': 'browser_extension' };
	var encodedQuery = encodeQueryData(queryData);
	
	// Find hRecipe by finding an element with hRecipe class
	function findHRecipe() {
		var siteUsesHRecipe = document.getElementsByClassName('hrecipe').length > 0 || false;
		return siteUsesHRecipe;
	}
	// Search to see if the page uses microformats
	function findMicroformat() {
		var schemaDotOrgElement = document.querySelectorAll('[itemtype="http://schema.org/Recipe"]');
		var dataVocabularyDotOrgElement = document.querySelectorAll('[itemtype="http://data-vocabulary.org/Recipe"]');
		var siteUsesMicroformat = schemaDotOrgElement.length > 0 || dataVocabularyDotOrgElement.length > 0;
		return siteUsesMicroformat;
	}
	// Loop through whitelist to find root url of site
	function findCurrentUrlInRecipeWhiteList() {
		var currentSite = appAPI.utils.getDomain(currentUrl);
		var siteInWhiteList = appAPI.utils.indexOf(whiteList, currentSite) !== -1;
		return siteInWhiteList;
	}
	
	function decideRootUrl() {
		if(usesHRecipe || usesMicroformat || isInWhiteList) {
			console.log("Choosing recipe page");
			rootUrl = "https://app.whisk.com/#/recipe?";
		} else {
			console.log("Choosing shopping list");
			rootUrl = "https://app.whisk.com/#/";
		}
		return rootUrl;
	}

	function encodeQueryData(data) {
		var queryString = [];
		for (var item in data)
			queryString.push(encodeURIComponent(item) + "=" + encodeURIComponent(data[item]));
		return queryString.join("&");
	}

	function getTargetUrl(currentUrl) {
		var finalUrl = rootUrl + encodedQuery;
		return finalUrl;
	}

	function loadSidebar() {
		var iframeUrl = getTargetUrl(currentUrl);

		sidebar = new appAPI.sidebar({
			position:'right',
			url: iframeUrl,
			opacity:1.0,
			width:'500px',
			height:'100%',
			preloader:true,
			sticky:true,
			slide:150,
			openAction:'click',
			closeAction:'click',
			scrollbars:true,
			openOnInstall:true,
			events:{
				onShow:function () {
					sidebarOpen = true;
				},
				onHide:function () {
					setTimeout(function () {
						sidebar.hide(true);	
					}, 820);
					sidebarOpen = false;
				}
			}
		});
		loadSidebarStyle();
	}
	
	// Load styles for sidebar (tacky but there's no easy way of doing this)
	function loadSidebarStyle() {
		$('.crossrider-sidebar-' + appId + '-label-theme-default').css({
			'background-image': "url('" + appAPI.resources.getImage("images/whisk_widget_close_app.png") + "')",
			'background-color': '#ebe6e0',
			'background-repeat': 'no-repeat',
			'background-position': 'center',
			'box-shadow': '-2px 1px 2px 0 #555',
			'background-size': '35px 50px',
			'border-bottom-left-radius': '25px',
			'border-top-left-radius': '25px',
			'height': '50px',
			'width': '40px'
		});
	
		$('.crossrider-sidebar-' + appId + '-content-theme-default').css({
			'box-shadow': '-2px 1px 2px 0 #555',
			'border': 'none',
			'border-radius': '0'
		});
	}
	
	// Check for click on plugin button
	appAPI.message.addListener(function(msg) {
		if (msg.action === 'openWhisk') {
			if (!sidebar) {
				loadSidebar();
			}

			if (sidebarOpen === false) {
				sidebar.show(true);
				sidebar.open();	
			} else if (sidebarOpen === true) {
				sidebar.close();
				setTimeout(function () {
					sidebar.hide(true);	
				}, 820);
			}
		}
	});
});