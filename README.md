## Running the extension

You'll need to have an account on crossrider, we'll probably need a generic Whisk account.

Create a new app if there isn't already one set up. Ensure you're on the **"Edit code"** tab, this is where you need to upload a zipped up version of the app. The files / folders you need to include are:

* extension.js
* background.js
* resources
* resources/icon.png
* resources/images/whisk_widget_close_app.png

### Uploading the project to crossrider

Uploading the zip file will override the current app on crossrider, so make sure you've not got any outstanding changes added using crossriders editor. It's going to be best to work locally and use version control to make life easier anyway.

In the left sidebar there's a button for uploading your project, use this to upload your zip file. When this is done, the project should update and you'll see that it's now using our zipped project.

### Install the crossrider sidebar plugin

Before installing our extension to the browser, we need to install crossriders sidebar plugin. This gives us access to use a simple sidebar which makes our life a hell of a lot easier as we just need to pass in a url. To install the sidebar plugin, make sure you're on the **Edit Code** page and in the left sidebar you'll see a list of files under **My project**. Under plugins, you can click **Add plugin** which will open a modal box where you'll just need to select the sidebar plugin.

### Enabling browser buttons

In order to actually have a browser button to click, we need to enable the option in the settings, so head over to the settings tab and click **Browser buttons** in the left sidebar. Then simply make sure each option is set to on and save your changes. You're now ready to install the actual plugin.

### Installing the extension to your browser

Again, in the left sidebar on the edit code page, there should be a green button "Install STAGING extn" You can also download the file from the settings page by looking at the bottom of the left sidebar where it says install extension. Clicking this will start a file download in Chrome. For other browsers like firefox it will just automatically install at this point.

To install the extension on chrome, you need to go to tools -> extensions. Once there, you just need to drag the downloaded file onto the extensions page (you'll know you're doing it right if it says "drop to install" while dragging the file). Click add and wait a few seconds and you should be redirected to a thankyou page. You should also now have a shiny new button to press (the Whisk logo).

### Running locally

To run locally, you'll need to go to the **Debug mode** page. Follow the instructions here. You shouldn't need to download the debug package, as it's just the same as the files you zipped up and uploaded. You should see a screen with **Local Development Base URL** as its title.

Right now, you need to go to terminal and cd into your extension directory. You'll need to run a server from this directory in order for crossrider to pick it up, the simplest option would be to run the following command in that directory:

```
#!

python -m SimpleHTTPServer
```

Which will allow you to access those files at http://localhost:8000. Return to the page asking for the local development base url and paste **http://localhost:8000** then click Save url.

The options on the right will now become active, and just to be sure, click both blue buttons which should refresh your code to make sure it's up to date.

You can check that your local changes work by quickly adding a console.log(); just underneath appAPI.ready in extensions.js

Open up the console in the browser, refresh the page and your new console.log message should appear.

Once we have a common Whisk account on crossrider, a lot of these steps won't need to be taken.